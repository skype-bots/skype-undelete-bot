package main

import (
	"fmt"
	"gitlab.com/gear54/go-skype/skype"
	"regexp"
	"strings"
	"time"
)

const commandHelp = `Available commands:
%s help - Prints this help text (private conversations only).

%s where - Prints the current conversation ID and topic. This is used for` +
	` conversation matching.

%s ping - Prints the current uptime and message TTL. Use this to check if` +
	` the bot is alive.

If none of the above is detected, the command is parsed as follows:

%s cConversation uUser v0:2 0:2

c - Select messages by conversation. This is only used when you query the` +
	` bot in private. Matches conversation ID or topic case-insensitively.` +
	` To get current group conversation data, use '%s where'.

u - Select messages by sender. Matches username or display name case-insensitively.` +
	` Specify several using comma. Default: all users.

v - Select message versions. Each new message, edit or removal is a message` +
	` version. Uses slice syntax (e.g. 0:2 matches 3 versions starting from the` +
	` earliest). Default: 0.

Last token (without prefix) selects messages. Uses the same semantics as the` +
	` version filter. 0 means the most recent message. Default: 0.

You can also select messages by quoting them, although this is not that precise.`

var rxSplitDurationParts = regexp.MustCompile(`([a-z]+)(\d)`)

func formatDuration(d time.Duration) string {
	return rxSplitDurationParts.ReplaceAllString(
		d.Round(1*time.Second).String(),
		`$1 $2`,
	)
}

const commandPrefix = "!ud"

func printHelp(message *skype.Message) {
	if !message.Conv.IsPersonal {
		message.Reply(
			skype.MsgText("Help text is very long, I'll reply if you ask in private."),
		)

		return
	}

	message.Reply(
		skype.MsgText(strings.Replace(commandHelp, "%s", commandPrefix, -1)),
	)
}

func printConvData(message *skype.Message, history History) {
	if message.Conv.IsPersonal {
		return
	}

	for conv := range history {
		if conv.ID == message.Conv.ID {
			message.Reply(
				skype.MsgText(
					fmt.Sprintf(
						"Conversation ID: %s, Topic: %s",
						conv.ID,
						conv.Topic,
					),
				),
			)

			return
		}
	}

	message.Reply(skype.MsgText("Can't display conversation data. Please try again."))
}

func printPing(message *skype.Message, upSince time.Time, ttl time.Duration) {
	message.Reply(
		skype.MsgText(
			fmt.Sprintf(
				"Heyo! Up: %s, messages are kept for: %s.",
				formatDuration(time.Now().Sub(upSince)),
				formatDuration(ttl),
			),
		),
	)
}

const maxResponseLength = 1000

var rxTokenize = regexp.MustCompile(`([^"]\S*|".+?")\s*`)

func (h History) processCommand(message *skype.Message, upSince time.Time, ttl time.Duration) {
	if message.Type != skype.MessageNew {
		return
	}

	var text skype.MsgText
	var quote *skype.MsgQuote
	var ok bool

	switch len(message.Content) {
	case 1:
		text, ok = message.Content[0].(skype.MsgText)
		if !ok {
			return
		}
	case 2:
		quoteVal, ok := message.Content[0].(skype.MsgQuote)
		if !ok {
			return
		}
		quote = &quoteVal

		text, ok = message.Content[1].(skype.MsgText)
		if !ok {
			return
		}
	default:
		return
	}

	str := text.Text()
	if !(strings.HasPrefix(str, commandPrefix+" ") || (str == commandPrefix)) {
		return
	}

	rawTokens := rxTokenize.FindAllStringSubmatch(str, -1)

	var tokens []string

	for _, match := range rawTokens {
		tokens = append(tokens, strings.Replace(match[1], `"`, "", -1))
	}

	// simple command
	if len(tokens) == 2 {
		switch tokens[1] {
		case "help":
			printHelp(message)

			return
		case "where":
			printConvData(message, h)

			return
		case "ping":
			printPing(message, upSince, ttl)

			return
		}
	}

	var (
		convToken  string
		userTokens []string
		verTokens  []string
		msgTokens  []string
	)

	for _, token := range tokens[1:] {
		switch token[0] {
		case 'c':
			convToken = token[1:]
		case 'u':
			userTokens = strings.Split(token[1:], ",")
		case 'v':
			verTokens = strings.Split(token[1:], ",")
		default:
			msgTokens = strings.Split(token, ",")
		}
	}

	vMessages, err := h.filter(message, convToken, userTokens, verTokens, msgTokens, quote)
	if err != nil {
		message.Reply(skype.MsgText(fmt.Sprintf("Nothing found: %s", err.Error())))

		return
	}

	var reply []skype.MessagePart

	for i := len(vMessages) - 1; i >= 0; i-- {
		messages := vMessages[i]
		shortID := messages.ShortID()

		for _, message := range messages {
			var typeText string

			switch message.Type {
			case skype.MessageNew:
				typeText = "(%s, new):"
			case skype.MessageEdit:
				typeText = "(%s, edit):"
			case skype.MessageRemoval:
				typeText = "- Message %s removed"
			}

			now := time.Now()
			sender := message.Sender.DisplayName

			if len(sender) == 0 {
				sender = message.Sender.ID
			}

			reply = append(
				reply,
				skype.MsgText(fmt.Sprintf(
					"[%s ago] %s %s\n",
					formatDuration(now.Sub(message.ComposeTime)),
					sender,
					fmt.Sprintf(typeText, shortID),
				)),
			)

			if len(message.Content) > 0 {
				reply = append(reply, message.Content...)
				reply = append(reply, skype.MsgText("\n"))
			}
		}
	}

	if !message.Conv.IsPersonal {
		length := 0

		for _, part := range reply {
			length += len([]rune(part.Text()))
		}

		if length > maxResponseLength {
			message.Reply(
				skype.MsgText(
					fmt.Sprintf(
						"Found too much (%d characters long, max %d). Ask me in"+
							" private, but specify the conversation.",
						length,
						maxResponseLength,
					),
				),
			)

			return
		}
	}

	message.Reply(reply...)

	return
}
