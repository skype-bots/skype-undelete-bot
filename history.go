package main

import (
	"encoding/binary"
	"encoding/hex"
	"gitlab.com/gear54/go-skype/skype"
	"strconv"
	"time"
)

const uint16Max = ^uint16(0)

type VersionedMessage []*skype.Message
type ConvMessages []VersionedMessage
type History map[*skype.Conversation]ConvMessages

func (vm VersionedMessage) ID() int64 {
	id, _ := strconv.ParseInt(vm[0].ID, 10, 64)

	return id
}

func (vm VersionedMessage) ShortID() string {
	bytes := []byte{0, 0}
	binary.LittleEndian.PutUint16(bytes, uint16(vm.ID()%int64(uint16Max)))

	return hex.EncodeToString(bytes)
}

func (vm VersionedMessage) ClientID() string {
	return vm[0].ClientID
}

func (vm VersionedMessage) Sender() *skype.User {
	return vm[0].Sender
}

func (vm VersionedMessage) ComposeTime() time.Time {
	return vm[0].ComposeTime
}

func (vm VersionedMessage) AddVersion(message *skype.Message) VersionedMessage {
	messageID, _ := strconv.ParseInt(message.ID, 10, 64)

	for key, version := range vm {
		versionID, _ := strconv.ParseInt(version.ID, 10, 64)

		if versionID > messageID {
			vm = append(vm, nil)
			copy(vm[key+1:], vm[key:])
			vm[key] = message

			return vm
		}
	}

	return append(vm, message)
}

func (h History) addMessage(message *skype.Message) {
	if message.Conv.IsPersonal {
		return
	}

	var conv *skype.Conversation

	for knownConv := range h {
		if knownConv.ID == message.Conv.ID {
			conv = knownConv

			break
		}
	}

	if conv == nil {
		conv = message.Conv
		conv.RefreshThread()
		h[conv] = ConvMessages{VersionedMessage{message}}

		return
	}

	convMessages := h[conv]
	messageID, _ := strconv.ParseInt(message.ID, 10, 64)
	insertIndex := -1

	for key, convMessage := range convMessages {
		if convMessage.ClientID() == message.ClientID {
			convMessages[key] = convMessages[key].AddVersion(message)

			return
		}

		if (insertIndex == -1) && (convMessage.ID() < messageID) {
			insertIndex = key
		}
	}

	if insertIndex != -1 {
		convMessages = append(convMessages, nil)
		copy(convMessages[insertIndex+1:], convMessages[insertIndex:])
		convMessages[insertIndex] = VersionedMessage{message}
		h[conv] = convMessages

		return
	}

	h[conv] = append(convMessages, VersionedMessage{message})
}

func (h History) trim(olderThan time.Time) {
	olderUnix := olderThan.UnixNano() / 1e6

	for conv, convMsg := range h {
		for msgKey, vMessage := range convMsg {
			if vMessage.ID() < olderUnix {
				h[conv] = append(convMsg[:msgKey], convMsg[msgKey+1:]...)

				if len(h[conv]) < 1 {
					delete(h, conv)
				}
			}
		}
	}
}
