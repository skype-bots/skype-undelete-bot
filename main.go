package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/howeyc/gopass"
	"gitlab.com/gear54/go-skype/skype"
)

const (
	help = `Usage: %s <Skype-login> <message-ttl>
You need to specify Skype login as the first argument. You will be prompted for a password.

Message TTL is the amount of time a message is remembered. Trimming happens only when another message arrives.
`

	timeFormat = "2006/01/02 15:04:05"
	status     = skype.StatusOnline
)

func main() {
	tr := skype.DefaultTransport()

	// Optional debugging mode (for use with something like Fiddler or mitmproxy)
	if len(os.Getenv("DEBUG")) > 0 {
		// Accept self-signed TLS certificates
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

		// Get proxy from HTTP_PROXY environment variable
		tr.Proxy = http.ProxyFromEnvironment
	}

	args := os.Args[1:]

	if len(args) == 0 {
		fmt.Printf(help, os.Args[0])

		return
	}

	login := args[0]                           // skype user to login as
	maintUsername := args[1]                   // maintenance skype user
	msgTTL, err := time.ParseDuration(args[2]) // message is cleared after this time

	fmt.Printf("Skype password for %s: ", login)
	password, err := gopass.GetPasswd()
	if err != nil {
		panic(err)
	}

	session, err := skype.Login(login, string(password), tr)
	if err != nil {
		panic(err)
	}

	maintUser, err := session.NewUser(maintUsername)
	if err != nil {
		panic(err)
	}

	session.SetStatus(status)
	loginData := session.LoginData()

	log.Printf(
		"Logged in as '%s', endpoint ID: %s, registration token expires at: %s\n",
		login,
		loginData.EndpointID,
		loginData.RegTokenExpires.Format(timeFormat),
	)

	var upSince time.Time
	history := History{}

	session.OnNewMessage(func(message *skype.Message, meta skype.EventMeta) {
		if message.Sender.ID == session.Me().ID {
			return
		}

		if (message.Type == skype.MessageNew) ||
			(message.Type == skype.MessageEdit) ||
			(message.Type == skype.MessageRemoval) {
			history.processCommand(message, upSince, msgTTL)
			history.addMessage(message)
			history.trim(time.Now().Add(-msgTTL))
		}
	})

	session.OnThreadUpdate(func(conv *skype.Conversation, _ skype.EventMeta) {
		for knownConv := range history {
			if knownConv.ID == conv.ID {
				knownConv.Topic = conv.Topic
				knownConv.Members = conv.Members
			}
		}
	})

	go func() {
		time.Sleep(1 * time.Minute)

		for conv := range history {
			time.Sleep(10 * time.Minute)

			err := conv.RefreshThread()
			if err != nil {
				log.Printf(
					"Error while refreshing conversation '%s': %s",
					conv.Topic,
					err.Error(),
				)
			}
		}
	}()

	// Refresh our registration token over N (N=5) tries spread evenly over previous
	// token TTL minus one minute. Attempts are spread like that to ensure any temporary
	// failures disappear until the next attempt. A minute is subtracted so that we have
	// some time on the last attempt before the token expires, this guarantees polling
	// without interruption.
	go func() {
		tryCount := 5
		expires := session.LoginData().RegTokenExpires

	nextUpdate:
		interval := expires.Add(-1*time.Minute).Sub(time.Now()) /
			time.Duration(tryCount)

		for i := 1; i <= tryCount; i++ {
			left := tryCount - i

			log.Printf(
				"Next registration token refresh at: %s\n",
				time.Now().Add(interval).Format(timeFormat),
			)

			time.Sleep(interval)

			log.Println("Refreshing registration token and ensuring endpoint availability...")

			// Re-authenticate and ensure endpoint availability, this gives us fresh registration
			// token with the new expiration date.
			err := session.EnsureEndpoint(true)
			if err != nil {
				var errMsg string

				if left > 0 {
					errMsg = fmt.Sprintf(
						"Failed to ensure endpoint availability (%d tries left): %s",
						left,
						err.Error(),
					)

					maintUser.SendMessage(skype.MsgText(errMsg))
					log.Println(errMsg)
				} else {
					errMsg = fmt.Sprintf(
						"Failed to ensure endpoint availability over %d tries, exiting",
						tryCount,
					)

					maintUser.SendMessage(skype.MsgText(errMsg))
					log.Println(errMsg)

					os.Exit(1)
				}

				continue
			}

			expires = session.LoginData().RegTokenExpires

			log.Printf(
				"Endpoint availability ensured successfully, registration token expires at: %s\n",
				expires.Format(timeFormat),
			)

			goto nextUpdate
		}
	}()

	// Handle SIGINT (^C) gracefully.
	interrupted := make(chan os.Signal, 1)
	signal.Notify(interrupted, os.Interrupt)

	go func() {
		<-interrupted
		log.Println("SIGINT received, logging out...")

		// Logout and exit.
		session.Logout()
		os.Exit(1)
	}()

	go func() {
		log.Println("Sending heartbeat...")

		for {
			time.Sleep(30 * time.Second)

			err := session.Activate(time.Minute)
			if err != nil {
				log.Printf(
					"Heartbeat error: %s\n",
					err.Error(),
				)
			}
		}
	}()

	upSince = time.Now()

	for {
		log.Println("Polling...")

		// Start polling for events and block.
		pollErr := session.StartPolling()
		resp := pollErr.Response()

		// If an error occurs, log it as well as the chunk of the response that caused it.
		log.Printf("Poll error: %s (chunk: %s, panic: %s)\n",
			pollErr.Error(),
			pollErr.Chunk(),
			pollErr.PanicValue(),
		)

		// If the Skype network returns non-200 HTTP code, it might mean something is wrong
		// with our endpoint. So we try to ensure its availability (without re-authentication)
		// over N (N=27) tries.
		if (resp != nil) && (resp.StatusCode != http.StatusOK) {
			log.Printf(
				"HTTP %s received during polling, response body:\n%s\n",
				resp.Status,
				pollErr.Body(),
			)

			tryCount := 27 // 27 attempts over the course of 6+ hours, intervals increase each time

			for i := 1; i <= tryCount; i++ {
				left := tryCount - i

				log.Println("Ensuring endpoint availability...")

				err := session.EnsureEndpoint(false)
				if err != nil {
					var errMsg string

					if left > 0 {
						errMsg = fmt.Sprintf(
							"Failed to ensure endpoint availability (%d tries left): %s",
							left,
							err.Error(),
						)

						maintUser.SendMessage(skype.MsgText(errMsg))
						log.Println(errMsg)
					} else {
						errMsg = fmt.Sprintf(
							"Failed to ensure endpoint availability over %d tries, exiting",
							tryCount,
						)

						maintUser.SendMessage(skype.MsgText(errMsg))
						log.Println(errMsg)

						// Logout and exit.
						session.Logout()

						return
					}

					nextTryIn := time.Duration(i) * time.Minute

					log.Printf(
						"Next attempt to ensure endpoint availability at: %s\n",
						time.Now().Add(nextTryIn).Format(timeFormat),
					)

					// Next try after 1 minute, then after 2, then 3 and up to N.
					time.Sleep(nextTryIn)

					continue
				}

				log.Println("Endpoint availability ensured successfully!")

				// Need to set status again since previous endpoint was probably lost.
				session.SetStatus(status)

				break
			}
		}
	}
}
