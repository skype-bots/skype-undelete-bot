package skype

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

const (
	// APIPathContacts gives a logged in user's contacts URL
	// when joined with APIPathMe.
	APIPathContacts = "/contacts"
)

// IsLegacyLogin checks if the Skype account with that login was created
// before MS took over. It determines that by checking '@outlook.com'
// suffix in the login.
func IsLegacyLogin(login string) bool {
	return !strings.HasSuffix(login, "@outlook.com")
}

// Status is a Skype network user status (like Online or Do not disturb).
type Status uint8

const (
	// StatusOnline represents Online status.
	StatusOnline Status = iota

	// StatusAway represents Away status.
	StatusAway

	// StatusDND represents Do not disturb (Busy) status.
	StatusDND

	// StatusInvisible represents Invisible (Hidden) status.
	StatusInvisible

	// StatusOffline represents Offline status.
	StatusOffline
)

var statusText = map[Status]string{
	StatusOnline:    "Online",
	StatusAway:      "Away",
	StatusDND:       "Busy",
	StatusInvisible: "Hidden",
	StatusOffline:   "Offline",
}

// StatusText returns a text that is used to represent user presence
// status on the Skype network.
func StatusText(status Status) string {
	return statusText[status]
}

// MarshalJSON is needed for JSON conversion.
func (s Status) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(StatusText(s))
	buffer.WriteString(`"`)

	return buffer.Bytes(), nil
}

// UnmarshalJSON is needed for JSON conversion.
func (s *Status) UnmarshalJSON(input []byte) error {
	var statusStr string

	err := json.Unmarshal(input, &statusStr)
	if err != nil {
		return err
	}

	ok := false

	for status, text := range statusText {
		if statusStr == text {
			*s = status
			ok = true
		}
	}

	if !ok {
		return fmt.Errorf(
			"unknown status text: %s",
			statusStr,
		)
	}

	return nil
}

// UserPresence contains information about user's online presence.
type UserPresence struct {
	// User is a pointer to the user which this presence is for.
	User *User

	// Status represents user's online status.
	Status Status
}

type userPresenceResource struct {
	Status Status `json:"status"`
}

func (u *User) userPresenceFromResource(resourceJSON json.RawMessage) (*UserPresence, error) {
	resource := userPresenceResource{}
	err := json.Unmarshal(resourceJSON, &resource)
	if err != nil {
		return nil, fmt.Errorf(
			"skype/user: failed to parse user presence resource: %s",
			err.Error(),
		)
	}

	return &UserPresence{User: u, Status: resource.Status}, nil
}

// TypeIDUser is a prefix that user objects have in IDs
// inside Skype network. A typical user ID in requests
// will look like 8:<username>.
const TypeIDUser = 8

// User represents a user on the Skype network. Its ID
// is the user's username. For non-legacy accounts this
// will include 'live:' prefix.
type User struct {
	Resource

	// DisplayName is a name that is visible in the Skype client's
	// main window as well as in conversations when the user sends
	// messages.
	DisplayName string
}

// Copy performs a deep copy of the User. The new User will refer
// to the same user on the Skype network.
func (u *User) Copy() *User {
	return &*u
}

// NetworkID returns a string that is used when specifying this resource
// in requests to Skype network. For User, it looks like: 8:<username>.
func (u *User) NetworkID() string {
	return fmt.Sprintf("%d:%s", TypeIDUser, u.ID)
}

// NewUser creates a new User for a given username. You can use this function
// to later call Refresh on the resulting User to retrieve information about
// arbitrary users.
func (s *Session) NewUser(username string) (*User, error) {
	res, err := s.newResource(username)
	if err != nil {
		return nil, err
	}

	return &User{Resource: *res}, nil
}

var (
	rxUserIDFromNetworkID = regexp.MustCompile(fmt.Sprintf(
		"%d:([^/]+)",
		TypeIDUser,
	))

	rxUserFromContactsURL = regexp.MustCompile(fmt.Sprintf(
		"%s/%s",
		APIPathMe+APIPathContacts,
		rxUserIDFromNetworkID.String(),
	))
)

// UserFromNetworkID constructs a User from network ID string that
// looks like: 8:<username>.
func (s *Session) UserFromNetworkID(networkID string) (*User, error) {
	match := rxUserIDFromNetworkID.FindStringSubmatch(networkID)
	if match == nil {
		return nil, fmt.Errorf("skype/user: failed to extract user ID from '%s'", networkID)
	}

	return s.NewUser(match[1])
}

// UserFromContactsURL constructs a User from the logged in user's contacts URL.
func (s *Session) UserFromContactsURL(resURL string) (*User, error) {
	parsedURL, err := url.Parse(resURL)
	if err != nil {
		return nil, err
	}

	match := rxUserFromContactsURL.FindStringSubmatch(parsedURL.Path)
	if match == nil {
		return nil, fmt.Errorf("skype/user: failed to extract user ID from '%s'", resURL)
	}

	return s.NewUser(match[1])
}

// Refresh the information about this user. This will
// attempt to fill all the fields it can using the information
// from Skype network.
func (u *User) Refresh() error {
	//u.Sess.Do()
	return nil
}

// Presence gets the UserPresence for this user.
func (u *User) Presence() (*UserPresence, error) {
	s := u.Sess

	req, _ := http.NewRequest(
		http.MethodGet,
		fmt.Sprintf(
			"%s/%s/%s",
			s.APIURLRoot()+APIPathMe+APIPathContacts,
			u.NetworkID(),
			APIPathPresence,
		),
		nil,
	)

	resp, err := s.Do(req)
	body, err := expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return nil, fmt.Errorf("skype/user: failed to get user presence: %s", err.Error())
	}

	presence, err := u.userPresenceFromResource(json.RawMessage(body))
	if err != nil {
		return nil, err
	}

	return presence, nil
}

// IsLegacy checks if the Skype user was created before MS took over.
// It determines that by checking 'live:' prefix in the username.
func (u *User) IsLegacy() bool {
	return !strings.HasPrefix(u.ID, "live:")
}

// Mention constructs message part that mentions the user.
// You can use that to send outgoing messages mentioning users.
//
// The Content property will be set to the user's DisplayName. If
// that is empty, a username will be used.
func (u *User) Mention() MsgMention {
	var content string

	if len(u.DisplayName) > 0 {
		content = u.DisplayName
	} else {
		content = u.ID
	}

	return MsgMention{u.ID, content}
}

// SendMessage is a convenience method for sending a message to the given
// user. It calls Conversation.SendMessage under the hood.
func (u *User) SendMessage(parts ...MessagePart) (time.Time, error) {
	conv, err := u.Sess.NewConversation(u.ID, true)
	if err != nil {
		return zeroTime, fmt.Errorf("skype/user: failed to create conversation: %s", err.Error())
	}

	return conv.SendMessage(parts...)
}
