package skype

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"time"

	"github.com/beevik/etree"
)

// QuoteTimeFormat is used when 'flattening' quotes (quoting a quote).
const QuoteTimeFormat = "2006/01/02 15:04:05"

// MessagePart represents a part of Skype message, like
// an emoticon or text. Implementing structures represent
// specific type of message part.
type MessagePart interface {
	// Text gives XML-free representation. This is used
	// when constructing quotes.
	Text() string

	// Raw is a representation as Skype gives it and how it
	// expects to receive it. With any special characters
	// and XML tags. This is used when sending
	// outgoing messages.
	Raw() string
}

// MsgText represents plain text part of message. To
// instantiate it, you can simply convert a string
// like this: MsgText(str).
type MsgText string

// Text returns a string constructed from MsgText.
func (t MsgText) Text() string { return string(t) }

// Raw returns a simple string, same as Text (because
// text parts are not encoded in any way).
func (t MsgText) Raw() string { return t.Text() }

// MsgAction represents a /me IRC-style action at the start
// of the message. Typically, this will be the first MessagePart
// in Content property. If it is, it means that the whole message
// was sent via /me. MsgAction is actually a string that contains
// display name of the user who created this action.
//
// You can convert any string to an action part but clients will
// typically override it before rendering. The only use in actually
// using non-empty string is for the messages starting with it to
// look pretty in Skype Web that uses the same user as the session
// on which SendMessage was called.
type MsgAction string

// Text returns a string constructed from MsgAction
// with a space appended at the end.
func (a MsgAction) Text() string { return string(a) + " " }

// Raw returns a simple string, same as Text (because
// message actions use offset outside of content as
// a signaling mechanism).
func (a MsgAction) Raw() string { return a.Text() }

// Offset returns a number that represents byte length of
// the value returned by Text.
//
// This number needs to be submitted as part of outgoing
// message JSON for Skype network to recognize the
// message as /me action.
func (a MsgAction) Offset() int { return len([]byte(a.Text())) }

// MsgEmoticon is a part of the message that is a Skype emoticon.
// Create emoticons with NewEmoticon.
type MsgEmoticon struct {
	// Type is an emoticon type. It specifies what icon will actually
	// be drawn in the client
	Type,

	// Content is a text 'inside' the emoticon. Some emoticons have
	// more than one way to write them. For example, writing ':D'
	// and '(laugh)' will produce the same emoticon. When the
	// emoticon written as ':D' is parsed, Type would be 'laugh'
	// and Content would be ':D'.
	Content string
}

// Text returns emoticon's Content.
func (e MsgEmoticon) Text() string { return e.Content }

// Raw returns string representation of an XML tag that
// denotes emoticon when sending or receiving messages. Example:
// <ss type="laugh">:D</ss>.
func (e MsgEmoticon) Raw() string {
	doc := etree.NewDocument()
	emoticon := doc.CreateElement("ss")
	emoticon.CreateAttr("type", e.Type)
	emoticon.SetText(e.Text())
	result, _ := doc.WriteToString()

	return result
}

// NewEmoticon creates an emoticon message part given a type.
// Type is a string like 'laugh' or 'cool'.
func NewEmoticon(eType string) MsgEmoticon {
	return MsgEmoticon{eType, fmt.Sprintf("(%s)", eType)}
}

// MsgEmoji represents part of the message that is a Skype emoji.
// In Skype clients, you cannot send anything in addition to
// an emoji in the same message, that restriction applies to
// this API too.
type MsgEmoji struct{}

// MsgFile represents a sent file.
type MsgFile struct{}

// MsgMention is a part of the message that mentions somebody. Create
// mentions with NewMention or User.Mention.
type MsgMention struct {
	// Username is the username of the mentioned user. Same format
	// as User.ID
	Username,

	// Content is a text 'inside' the mention. You can set this to
	// custom values to customize mention visible text.
	Content string
}

// Text returns mention's Content.
func (m MsgMention) Text() string { return m.Content }

// Raw returns string representation of an XML tag that
// denotes mention when sending or receiving messages. Example:
// <at id="8:test">Test User</at>.
func (m MsgMention) Raw() string {
	doc := etree.NewDocument()
	mention := doc.CreateElement("at")
	mention.CreateAttr("id", fmt.Sprintf("%d:%s", TypeIDUser, m.Username))
	mention.SetText(m.Text())
	result, _ := doc.WriteToString()

	return result
}

// NewMention creates a mention of the provided username. It
// sets the Content property to the same value.
func NewMention(username string) MsgMention {
	return MsgMention{username, username}
}

// MsgAnchor is a part of the message that mentions somebody. Create
// mentions with NewAnchor. Note that Skype network will automatically
// create anchors out of any text that is a valid a HTTP(S) URL when
// sending a message even if you don't create the anchor yourself.
type MsgAnchor struct {
	// Href is the URL that the anchor opens when clicked
	Href,

	// Content is a text 'inside' the anchor. You can set this to
	// custom values to customize anchor visible text.
	Content string
}

// Text returns anchor's Content.
func (a MsgAnchor) Text() string { return a.Content }

// Raw returns string representation of an XML tag that
// denotes emoticon when sending or receiving messages. Example:
// <a href="http://example.com">Test User</at>.
func (a MsgAnchor) Raw() string {
	doc := etree.NewDocument()
	anchor := doc.CreateElement("a")
	anchor.CreateAttr("href", a.Href)
	anchor.SetText(a.Text())
	result, _ := doc.WriteToString()

	return result
}

// NewAnchor creates an anchor with the specified target URL. It
// sets the Content property to the same value.
func NewAnchor(href string) MsgAnchor {
	return MsgAnchor{href, href}
}

// MsgQuote represents part of the message that is a quote of
// some other message (or a part of it). It contains data about
// the time the original message was sent as well as who sent it.
//
// Since it is a message part that represents other message, it
// shares RawContent and Content properties with Message.
//
// Quotes are created with NewQuote or Message.Quote.
type MsgQuote struct {
	// Author is the User who originally sent the quoted
	// message or a part of message.
	Author *User

	// Timestamp is a time when the quoted content was sent.
	Timestamp time.Time

	// RawContent is a string that represents raw content of the original message.
	RawContent string

	// Content is a parsed content of the original message. It is an array of types
	// that implement MessagePart (implementing types are usually named 'Msg*').
	Content []MessagePart
}

func (q MsgQuote) quoteJoin(raw bool) string {
	var content bytes.Buffer

	for _, part := range q.Content {
		if raw {
			content.WriteString(part.Raw())
		} else {
			content.WriteString(part.Text())
		}
	}

	return content.String()
}

// Text returns a 'flattened' representation of a quote. It is
// intentionally not the same as flattened representation Skype
// client gives when quoting a quote because that is quite limited.
//
// Quote content is prefixed with time (check QuoteTimeFormat) and author's
// DisplayName (or ID if not available). The newline is appended at the end.
func (q MsgQuote) Text() string {
	var author string

	if q.Author != nil {
		if len(q.Author.DisplayName) > 0 {
			author = q.Author.DisplayName
		} else {
			author = q.Author.ID
		}
	}

	return fmt.Sprintf(
		"[%s] %s: %s\n",
		q.Timestamp.Format(QuoteTimeFormat),
		author,
		q.quoteJoin(false),
	)
}

// Raw constructs a small XML document that when sent to Skype
// network will be interpreted as quote of another message.
func (q MsgQuote) Raw() string {
	var author, authorName string

	if q.Author != nil {
		author = q.Author.ID
		authorName = q.Author.DisplayName
	}

	doc := etree.NewDocument()
	quote := doc.CreateElement("quote")

	quote.CreateAttr("author", author)
	quote.CreateAttr("authorname", authorName)
	quote.CreateAttr("timestamp", strconv.FormatInt(q.Timestamp.Unix(), 10))

	quote.CreateElement("legacyquote").SetText(" ") // don't ask :)
	quote.CreateCharData(q.quoteJoin(false))
	quote.CreateElement("legacyquote")

	result, _ := doc.WriteToString()

	return result
}

// NewQuote creates a quote given an author, timestamp
// and MessageParts.
//
// Quotes created this way will have their RawContent
// filled with concatenation of Raw representations of each
// MessagePart.
func NewQuote(author *User, timestamp time.Time, parts ...MessagePart) *MsgQuote {
	quote := &MsgQuote{
		author,
		timestamp,
		"",
		parts,
	}

	quote.RawContent = quote.quoteJoin(true)

	return quote
}

func (s *Session) parseMsgPartElement(element *etree.Element) MessagePart {
	switch element.Tag {
	case "ss":
		return MsgEmoticon{
			element.SelectAttrValue("type", ""),
			element.Text(),
		}
	case "quote":
		result := MsgQuote{}

		author, _ := s.NewUser(element.SelectAttrValue("author", ""))
		if author != nil {
			author.DisplayName = element.SelectAttrValue("authorname", "")
		}

		result.Author = author

		timestamp, err := strconv.ParseInt(element.SelectAttrValue("timestamp", "0"), 10, 64)
		if err != nil {
			timestamp = 0
		}

		result.Timestamp = time.Unix(timestamp, 0)

		doc := etree.NewDocument()
		element = element.Copy()
		doc.SetRoot(element)

		first, ok1 := element.Child[0].(*etree.Element)
		lastIndex := len(element.Child) - 1
		last, ok2 := element.Child[lastIndex].(*etree.Element)

		element.RemoveChild(element.Child[0])
		element.RemoveChild(element.Child[lastIndex-1]) // -1 because we removed one before
		result.RawContent, _ = doc.WriteToString()

		if ok1 && ok2 && (first.Tag == "legacyquote") && (last.Tag == "legacyquote") {
			result.Content = s.parseMessageContent(element.Child)
		}

		return result
	case "at":
		mention := MsgMention{Content: element.Text()}

		match := rxUserIDFromNetworkID.FindStringSubmatch(element.SelectAttrValue("id", ""))
		if match != nil {
			mention.Username = match[1]
		}

		return mention
	case "a":
		return MsgAnchor{
			Href:    element.SelectAttrValue("href", ""),
			Content: element.Text(),
		}
	default:
		return nil
	}
}

// MessageType represents a type of the message event.
type MessageType uint8

const (
	// MessageUnknown is a 'default' message type. Messages created
	// from bare IDs have this type.
	MessageUnknown MessageType = iota

	// MessageStartTyping indicates that the user started typing in
	// the conversation. Skype clients display a little pencil when they
	// receive that message.
	MessageStartTyping

	// MessageStopTyping indicates that the user stopped typing in
	// the conversation. Skype clients display a little pencil trying
	// to erase its writing when they receive that message.
	MessageStopTyping

	// MessageNew represents a simple message that the user has sent
	// into the conversation.
	MessageNew

	// MessageEdit is a message event that Skype network sends when
	// the user edits their previously sent message.
	MessageEdit

	// MessageRemoval is a message event that Skype network sends when
	// the user removes their previously sent message.
	MessageRemoval
)

// Message represents a message event in the Skype network. A message event
// is one of the following: user typing, new message being sent, message being edited or
// message being removed. Use NewMessage to instantiate this struct.
type Message struct {
	Resource

	// Type represents a message type (like message edit, or message removal). Check
	// MessageType enum for all possible values.
	Type MessageType

	// Sender is the User who sent the message (or performed an edit or removal).
	Sender *User

	// Conv is a conversation where the message was sent.
	Conv *Conversation

	// ComposeTime is a time that is sent in 'composetime' JSON field by the Skype network.
	ComposeTime time.Time

	// ClientID is a unique ID that is the same for message and all its modifications. You
	// can match MessageEdit and MessageRemoval messages with the appropriate MessageNews
	// using this field.
	ClientID,

	// RawContent is a string that represents raw content of the received message. The
	// editing prefix at the start and editing mark at the end are stripped if present.
	RawContent string

	// Content is a parsed content of the received message. It is an array of types
	// that implement MessagePart (implementing types are usually named 'Msg*').
	Content []MessagePart
}

// NewMessage creates a new Message for a given username. You can use this function to
// later call Refresh on the resulting struct to retrieve information about
// arbitrary messages.
func (c *Conversation) NewMessage(id string) (*Message, error) {
	if c == nil {
		return nil, errors.New("skype/message: conversation pointer cannot be nil")
	}

	r, err := c.Sess.newResource(id)
	if err != nil {
		return nil, err
	}

	return &Message{Resource: *r, Conv: c}, nil
}

var rxMessageFromConvURL = regexp.MustCompile(fmt.Sprintf(
	`%s/messages/(\d+)`,
	rxConversationFromURL.String(),
))

// MessageFromConvURL constructs a Message struct from the logged in
// user's messages for conversation URL.
func (s *Session) MessageFromConvURL(resURL string) (*Message, error) {
	parsedURL, err := url.Parse(resURL)
	if err != nil {
		return nil, err
	}

	conv, err := s.ConversationFromURL(resURL)
	if err != nil {
		return nil, fmt.Errorf(
			"skype/message: failed to create conversation for message: %s",
			err.Error(),
		)
	}

	match := rxMessageFromConvURL.FindStringSubmatch(parsedURL.Path)
	if match == nil {
		return nil, fmt.Errorf(
			"skype/message: failed to extract message ID from '%s'",
			resURL,
		)
	}

	return conv.NewMessage(match[len(match)-1])
}

type messageResource struct {
	Topic        string    `json:"threadtopic"`
	Type         string    `json:"messagetype"`
	SenderURL    string    `json:"from"`
	ID           string    `json:"id"`
	ClientID     string    `json:"clientmessageid"`
	EditID       string    `json:"skypeeditedid"`
	EditOffset   string    `json:"skypeeditoffset"`
	EmoteOffset  string    `json:"skypeemoteoffset"`
	ConvURL      string    `json:"conversationLink"`
	DisplayName  string    `json:"imdisplayname"`
	TypingStatus string    `json:"Skype-TypingStatus"`
	Content      string    `json:"content"`
	ComposeTime  time.Time `json:"composetime"`
}

func (s *Session) parseMessageContent(tokens []etree.Token) []MessagePart {
	var result []MessagePart

	for _, child := range tokens {
		switch token := child.(type) {
		case *etree.CharData:
			result = append(result, MsgText(token.Data))
		case *etree.Element:
			msgPart := s.parseMsgPartElement(token)

			if msgPart != nil {
				result = append(result, msgPart)
			}
		}
	}

	return result
}

func (s *Session) messageFromResource(resourceJSON json.RawMessage) (*Message, error) {
	resource := messageResource{}
	err := json.Unmarshal(resourceJSON, &resource)
	if err != nil {
		return nil, fmt.Errorf("skype/message: failed to parse message resource: %s", err.Error())
	}

	message := Message{
		Resource: Resource{
			Sess: s,
			ID:   resource.ID,
		},
		ComposeTime: resource.ComposeTime,
	}

	switch resource.Type {
	case "Text":
		fallthrough
	case "RichText":
		fallthrough
	case "RichText/Media_FlikMsg":
		fallthrough
	case "RichText/Media_GenericFile":
		message.Type = MessageUnknown // gets filled later
	case "Control/Typing":
		message.Type = MessageStartTyping
	case "Control/ClearTyping":
		message.Type = MessageStopTyping
	default:
		return nil, fmt.Errorf("skype/message: unknown 'messagetype': %s", resource.Type)
	}

	sender, err := s.UserFromContactsURL(resource.SenderURL)
	if err != nil {
		return nil, fmt.Errorf("skype/message: failed to determine message sender: %s", err.Error())
	}

	sender.DisplayName = resource.DisplayName
	message.Sender = sender

	conv, err := s.ConversationFromURL(resource.ConvURL)
	if err != nil {
		return nil, fmt.Errorf("skype/message: failed to determine conversation for message: %s", err.Error())
	}

	conv.Topic = resource.Topic
	message.Conv = conv

	if message.Type == MessageUnknown {
		doc := etree.NewDocument()
		doc.ReadFromString(resource.Content)

		if len(resource.EditID) > 0 {
			message.ClientID = resource.EditID

			if len(resource.EditOffset) > 0 {
				offset, err := strconv.ParseUint(resource.EditOffset, 10, 8)
				if err != nil {
					return nil, fmt.Errorf("skype/message: failed to parse edit prefix length: %s", err.Error())
				}

				editPrefix, prefixOK := doc.Child[0].(*etree.CharData)

				if !prefixOK {
					return nil, errors.New(
						"skype/message: failed to find edit prefix, first message element isn't a string",
					)
				}

				if len(editPrefix.Data) < int(offset) {
					return nil, fmt.Errorf(
						"skype/message: failed to find edit prefix (first string length: %d, expected: %d)",
						len(editPrefix.Data),
						offset,
					)
				}

				editPrefix.Data = editPrefix.Data[offset:]

				if len(editPrefix.Data) == 0 {
					doc.RemoveChild(editPrefix)
				}
			}

			if len(doc.Child) > 0 {
				editMark, markOK := doc.Child[len(doc.Child)-1].(*etree.Element)

				if !(markOK && (editMark.Tag == "e_m")) {
					return nil, errors.New("skype/message: failed to find message edit mark")
				}

				// If we want some data from edit mark, now is the time
				doc.RemoveChild(editMark)
			}

			if len(doc.Child) > 0 {
				message.Type = MessageEdit
			} else {
				message.Type = MessageRemoval
			}
		} else {
			message.ClientID = resource.ClientID
			message.Type = MessageNew
		}

		if len(resource.EmoteOffset) > 0 {
			offset, err := strconv.ParseUint(resource.EmoteOffset, 10, 8)
			if err != nil {
				return nil, fmt.Errorf("skype/message: failed to parse action offset: %s", err.Error())
			}

			msgAction, actionOk := doc.Child[0].(*etree.CharData)

			if !actionOk {
				return nil, errors.New(
					"skype/message: failed to find message action, first message element isn't a string",
				)
			}

			dataBytes := []byte(msgAction.Data)

			if len(dataBytes) < int(offset) {
				return nil, fmt.Errorf(
					"skype/message: failed to find message action (first string byte length: %d, expected: %d)",
					len(dataBytes),
					offset,
				)
			}

			actionPart := MsgAction(dataBytes[:offset-1]) // subtract 1 to shave off the space at the end
			dataBytes = dataBytes[offset:]

			if len(dataBytes) == 0 {
				doc.RemoveChild(msgAction)
			} else {
				msgAction.Data = string(dataBytes)
			}

			message.Content = []MessagePart{actionPart}
		}

		message.RawContent, _ = doc.WriteToString()
		message.Content = append(message.Content, s.parseMessageContent(doc.Child)...)
	}

	return &message, nil
}

type outgoingMessageResource struct {
	Content     string `json:"content"`
	Type        string `json:"messagetype"`
	ClientID    int64  `json:"clientmessageid,string"`
	EmoteOffset int    `json:"skypeemoteoffset,omitempty"`
}

// Construct a JSON text that can be sent ot Skype network to send a message.
func outgoingMessageJSON(parts ...MessagePart) string {
	message := outgoingMessageResource{
		Type:     "RichText",
		ClientID: time.Now().UnixNano() / 1e6,
	}

	var buffer bytes.Buffer

	if len(parts) > 0 {
		if action, ok := parts[0].(MsgAction); ok {
			message.EmoteOffset = action.Offset()
		}
	}

	for _, part := range parts {
		buffer.WriteString(part.Raw())
	}

	message.Content = buffer.String()
	messageJSON, _ := json.Marshal(message)

	return string(messageJSON)
}

// Refresh the information about this message event. This will
// attempt to fill all the fields it can using the information
// from Skype network.
func (m *Message) Refresh() error {
	//u.Sess.Do()
	return nil
}

// Text is a convenience function for getting message content in plaintext form.
func (m *Message) Text() string {
	var text bytes.Buffer

	for _, part := range m.Content {
		text.WriteString(part.Text())
	}

	return text.String()
}

// Quote is a convenience function for constructing a MsgQuote out of a Message.
func (m *Message) Quote() *MsgQuote {
	return NewQuote(m.Sender, m.ComposeTime, m.Content...)
}

// Reply is a convenience function for sending a new message to
// the same conversation which the original message is part of.
func (m *Message) Reply(message ...MessagePart) (time.Time, error) {
	return m.Conv.SendMessage(message...)
}
