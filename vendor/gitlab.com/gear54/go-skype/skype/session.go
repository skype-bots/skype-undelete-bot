package skype

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	// UserAgent is the user agent that this library uses when talking to Skype network.
	UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) " +
		"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.106 Safari/537.36"

	// APIHost holds Skype network API host for non-legacy accounts (see IsLegacyLogin). This
	// is a default partial value, use Session.APIURLRoot to obtain actual API URL that the
	// session will use for its requests.
	APIHost = "client-s.gateway.messenger.live.com"

	// APIPathMe is the root of the logged in user's resources.
	APIPathMe = "/users/ME"

	// APIPathEndpoints gives a logged in user's endpoints URL
	// when joined with APIPathMe.
	APIPathEndpoints = "/endpoints"

	// APIPathSelf gives a logged in user's endpoint URL
	// when joined with APIPathMe and APIPathEndpoints.
	APIPathSelf = "/SELF"

	// APIPathActive gives an endpoint's activation URL
	// when joined with endpoint URL.
	APIPathActive = "/active"

	// APIPathPresence gives a logged in user's presence
	// URL when joined with APIPathMe and endpoint or contact URLs.
	APIPathPresence = "/presenceDocs/messagingService"

	// APIPathSubscriptions gives endpoint's subscriptions URL
	// when joined with endpoint URL.
	APIPathSubscriptions = "/subscriptions"

	// APIPathPoll gives a logged in user's first subscription polling URL
	// when joined with APIPathMe, APIPathEndpoints, APIPathSelf and APIPathSubscriptions.
	APIPathPoll = "/0/poll"
)

// LoginData holds various pieces of data received during the login process and
// endpoint creation.
//
// This is useful if you want to perform actions that are not implemented
// in this library. Session.APIURLRoot function and APIPath* constants will
// help you with this as well.
type LoginData struct {
	SkypeToken,
	RegToken string
	RegTokenExpires time.Time
	EndpointID      string
}

// Session represents a session in the Skype network. It holds all
// authentication data obtained during login precess and allows
// you to perform any action a web version of Skype is able to
// perform after logging in via login and password.
//
// The Session itself is safe for concurrent use, but the objects
// you will receive when working with it are not.
type Session struct {
	apiURL string
	client *http.Client
	me     *User

	mu             sync.RWMutex
	loginData      LoginData
	cancelPolling  context.CancelFunc
	onUserPresence UserPresenceHandler
	onMessage      MessageHandler
	onThreadUpdate ThreadUpdateHandler
}

func sessionErrorF(format string, a ...interface{}) error {
	return fmt.Errorf(fmt.Sprintf("skype/session: %s", format), a...)
}

// IsLoggedIn indicates whether or not a session is logged in.
//
// A session is considered to be logged in if it has non-empty
// non-expired registration token.
//
// Note that almost all Session actions can only be performed
// when the session is logged in. It is up to you to check that
// using this function if you are not sure about the state of
// the session.
func (s *Session) IsLoggedIn() bool {
	s.mu.RLock()
	loginData := s.loginData
	s.mu.RUnlock()

	return len(s.loginData.RegToken) > 0 &&
		time.Now().Before(loginData.RegTokenExpires)
}

// APIURLRoot returns Skype network root API URL for a given
// session. This differs based on whether or not the account is
// legacy (see IsLegacyLogin).
//
// Session will determine the URL it needs to use during login
// process and then will use it for all further communications.
func (s *Session) APIURLRoot() string {
	return s.apiURL
}

// LoginData returns login data struct that holds all tokens
// obtained throughout the Skype network login process and
// endpoint creation.
func (s *Session) LoginData() LoginData {
	s.mu.RLock()
	result := s.loginData
	s.mu.RUnlock()

	return result
}

// Client returns a pointer to the http.Client that the session uses for all
// its requests. This is useful if you want to adjust its parameters (like
// proxying or TLS configuration).
func (s *Session) Client() *http.Client {
	return s.client
}

// Me returns a pointer to copy of the logged in User. Be sure to store
// the returned pointer somewhere if you need persistence (e.g. for Refresh).
func (s *Session) Me() *User {
	return s.me.Copy()
}

func (s *Session) do(req *http.Request, skypeToken, regToken string) (*http.Response, error) {
	req.Header.Set("User-Agent", UserAgent)

	if len(skypeToken) > 0 {
		req.Header.Set(
			"Authentication",
			fmt.Sprintf("skypetoken=%s", skypeToken),
		)
	} else if len(regToken) > 0 {
		req.Header.Set(
			"RegistrationToken",
			fmt.Sprintf("registrationToken=%s", regToken),
		)
	}

	return s.client.Do(req)
}

// Do mimics http.Client.Do. Copies the given request,
// appends RegistrationToken header (if the session has it)
// and executes the request via session's HTTP client.
func (s *Session) Do(req *http.Request) (*http.Response, error) {
	regToken := s.LoginData().RegToken

	return s.do(req, "", regToken)
}

// Post mimics http.Client.Post. It uses Session.Do under the hood.
func (s *Session) Post(url string, contentType string, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", contentType)

	return s.Do(req)
}

// PostForm mimics http.Client.PostForm. It uses Session.Do under the hood.
func (s *Session) PostForm(url string, data url.Values) (resp *http.Response, err error) {
	return s.Post(url, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
}

type endpointActiveResource struct {
	Timeout int `json:"timeout"`
}

// Activate is a keep-alive function. Calling this periodically (with a period
// that is less than timeout argument) is required to keep the endpoint available
// for polling if you are not currently polling.
//
// Maximum value for a timeout is 5 minutes.
func (s *Session) Activate(timeout time.Duration) error {
	if timeout < 0 {
		return sessionErrorF("timeout is less than 0")
	}
	if timeout > (300 * time.Second) {
		return sessionErrorF("timeout is bigger than allowed 300 seconds")
	}

	reqBody, _ := json.Marshal(endpointActiveResource{
		Timeout: int(timeout / time.Second),
	})

	req, _ := http.NewRequest(
		http.MethodPost,
		s.APIURLRoot()+APIPathMe+APIPathEndpoints+
			APIPathSelf+APIPathActive,
		bytes.NewReader(reqBody),
	)

	resp, err := s.Do(req)
	_, err = expectCodeGetBody(http.StatusCreated, resp, err)
	if err != nil {
		return sessionErrorF(err.Error())
	}

	return nil
}

const (
	endpointCreateBody = `{"endpointFeatures":"Agent"}`

	subscribeBody = `{
  "channelType": "httpLongPoll",
  "template": "raw",
  "interestedResources": [
    "/v1/users/ME/conversations/ALL/properties",
    "/v1/users/ME/conversations/ALL/messages",
    "/v1/users/ME/contacts/ALL",
    "/v1/threads/ALL"
  ]
}`

	createPresenceBody = `{
  "id": "messagingService",
  "type": "EndpointPresenceDoc",
  "selfLink": "uri",
  "privateInfo": {
    "epname": "skype"
  },
  "publicInfo": {
    "capabilities": "video|audio",
    "type": 1,
    "skypeNameVersion": "skype.com",
    "nodeInfo": "",
    "version": "908/1.115.0.27//skype.com"
  }
}`
)

func (s *Session) setRegToken(headers http.Header) error {
	match := rxRegistrationToken.FindStringSubmatch(headers.Get("Set-Registrationtoken"))
	if match == nil {
		return errors.New("failed to parse registration token header")
	}

	expires, err := strconv.ParseInt(match[2], 10, 64)
	if err != nil {
		return fmt.Errorf(
			"failed to parse registration token expiration time: %s",
			err.Error(),
		)
	}

	s.mu.Lock()
	s.loginData.RegToken = match[1]
	s.loginData.RegTokenExpires = time.Unix(expires, 0)
	s.loginData.EndpointID = match[3]
	s.mu.Unlock()

	return nil
}

type endpointResource struct {
	Subscriptions []json.RawMessage `json:"subscriptions"`
}

func (s *Session) ensureEndpoint(reAuth, reqUpdate bool) error {
	update := reAuth || reqUpdate
	subscribe := true

	if update {
		if reAuth {
			req, _ := http.NewRequest(http.MethodGet, LoginURL, nil)

			resp, err := s.do(req, "", "")
			err = s.refreshSkypeToken(resp, err)
			if err != nil {
				return sessionErrorF(
					"failed to refresh Skype token (perhaps cookies have expired): %s",
					err.Error(),
				)
			}
		}

		loginData := s.LoginData()

		req, _ := http.NewRequest(
			http.MethodPut,
			fmt.Sprintf(
				"%s/{%s}",
				s.APIURLRoot()+APIPathMe+APIPathEndpoints,
				loginData.EndpointID,
			),
			strings.NewReader(endpointCreateBody),
		)

		var (
			resp *http.Response
			err  error
		)

		if reAuth {
			resp, err = s.do(req, loginData.SkypeToken, "")
		} else {
			resp, err = s.do(req, "", loginData.RegToken)
		}

		body, err := expectCodeGetBody(http.StatusOK, resp, err)
		if err != nil {
			return sessionErrorF(
				"failed to update endpoint: %s",
				err.Error(),
			)
		}

		// Refresh registration token if possible, but ignore if it isn't there
		s.setRegToken(resp.Header)

		resource := endpointResource{}
		err = json.Unmarshal([]byte(body), &resource)
		if err != nil {
			return sessionErrorF(
				"failed to parse endpoint resource: %s",
				err.Error(),
			)
		}

		subscribe = len(resource.Subscriptions) == 0
	}

	if subscribe {
		req, _ := http.NewRequest(
			http.MethodPost,
			s.APIURLRoot()+APIPathMe+APIPathEndpoints+
				APIPathSelf+APIPathSubscriptions,
			strings.NewReader(subscribeBody),
		)

		resp, err := s.Do(req)
		_, err = expectCodeGetBody(http.StatusCreated, resp, err)
		if err != nil {
			return sessionErrorF(fmt.Sprintf(
				"failed to subscribe to HTTP long poll: %s",
				err.Error(),
			))
		}
	}

	req, err := http.NewRequest(
		http.MethodPut,
		s.APIURLRoot()+APIPathMe+APIPathEndpoints+
			APIPathSelf+APIPathPresence,
		strings.NewReader(createPresenceBody),
	)

	resp, err := s.Do(req)
	_, err = expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return sessionErrorF(fmt.Sprintf(
			"failed to update endpoint presence: %s",
			err.Error(),
		))
	}

	return nil
}

// EnsureEndpoint does several things to ensure the availability of
// the session's endpoint for immediate polling. Setting reAuth to true
// will also refresh your registration token using cookies obtained during
// login process.
//
// You can use this if StartPolling returned some non-200 HTTP code to
// avoid creating the Session from scratch.
func (s *Session) EnsureEndpoint(reAuth bool) error {
	err := s.ensureEndpoint(reAuth, true)
	if err != nil {
		return err
	}

	return nil
}

// Status is a convenience function to get the logged in user's
// status from the Skype network.
func (s *Session) Status() (Status, error) {
	presence, err := s.me.Presence()
	if err != nil {
		return StatusOffline, fmt.Errorf(
			"skype/session: failed to get logged in user's status: %s",
			err.Error(),
		)
	}

	return presence.Status, nil
}

type setStatusResource struct {
	Status Status `json:"status"`
}

// SetStatus allows you to change the logged in user's status
// in the Skype network.
//
// You can't use StatusOffline here for some reason.
func (s *Session) SetStatus(status Status) (*UserPresence, error) {
	reqBody, _ := json.Marshal(setStatusResource{status})

	req, _ := http.NewRequest(
		http.MethodPut,
		s.APIURLRoot()+APIPathMe+APIPathPresence,
		bytes.NewReader(reqBody),
	)

	resp, err := s.Do(req)
	body, err := expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return nil, fmt.Errorf("skype/session: failed to set status: %s", err.Error())
	}

	presence, err := s.me.userPresenceFromResource(json.RawMessage(body))
	if err != nil {
		return nil, fmt.Errorf("skype/session: failed to parse updated user presence: %s", err.Error())
	}

	return presence, nil
}
