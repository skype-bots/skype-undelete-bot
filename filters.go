package main

import (
	"bytes"
	"errors"
	"gitlab.com/gear54/go-skype/skype"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func strFilterEmpty(arr []string) []string {
	var result []string

	for _, str := range arr {
		if len(str) > 0 {
			result = append(result, str)
		}
	}

	return result
}

func filterConv(h History, convToken string, message *skype.Message) []VersionedMessage {
	var messages []VersionedMessage

	if len(convToken) == 0 {
		return nil
	}

	tokenLower := strings.ToLower(convToken)

includeConv:
	for conv, convMessages := range h {
		if strings.Contains(strings.ToLower(conv.ID), tokenLower) ||
			strings.Contains(strings.ToLower(conv.Topic), tokenLower) {
			for member := range conv.Members {
				if member.ID == message.Sender.ID {
					messages = append(messages, convMessages...)
					break includeConv
				}
			}
		}
	}

	return messages
}

func filterUser(messages []VersionedMessage, userTokens []string) []VersionedMessage {
	var result []VersionedMessage

	for _, message := range messages {
		if len(userTokens) == 0 {
			result = append(result, message)

			continue
		}

		sender := message.Sender()

		for _, token := range userTokens {
			tokenLower := strings.ToLower(token)

			if strings.Contains(strings.ToLower(sender.DisplayName), tokenLower) ||
				strings.Contains(sender.ID, tokenLower) {
				result = append(result, message)
			}
		}
	}

	return result
}

var rxSliceSpec = regexp.MustCompile(`(-?\d+)?(:)?(-?\d+)?`)

func sliceParts(spec string, arr []interface{}) []interface{} {
	var checkIndex = func(maxIndex, index int64) bool {
		if (index < 0) || (index > maxIndex) {
			return true
		}

		return false
	}

	var result []interface{}

	specParts := rxSliceSpec.FindStringSubmatch(spec)
	if specParts == nil {
		return result
	}

	maxIndex := int64(len(arr)) - 1
	start, err := strconv.ParseInt(specParts[1], 10, 8)
	if err == nil {
		if specParts[1][0] == '-' {
			start = maxIndex - start
		}
		if checkIndex(maxIndex, start) {
			return nil
		}
	} else {
		start = 0
	}

	end, err := strconv.ParseInt(specParts[3], 10, 8)
	if err == nil {
		if specParts[3][0] == '-' {
			end = maxIndex - end
		}
		if checkIndex(maxIndex, end) {
			return nil
		}
	} else {
		end = maxIndex
	}

	if (len(specParts[2]) > 0) && (start != end) {
		if start < end {
			for i := start; i <= end; i++ {
				result = append(result, arr[i])
			}
		} else {
			for i := end; i <= start; i++ {
				result = append(result, arr[i])
			}
		}
	} else {
		result = append(result, arr[start])
	}

	return result
}

func filterMsgQuote(messages []VersionedMessage, quote *skype.MsgQuote) []VersionedMessage {
	var result []VersionedMessage

	var quoteStr bytes.Buffer

	for _, part := range quote.Content {
		quoteStr.WriteString(part.Text())
	}

	howMany := len(strFilterEmpty(strings.Split(quoteStr.String(), "\n")))

	for i := len(messages) - 1; (i != 0) && (howMany > 0); i-- {
		message := messages[i]

		if message.ComposeTime().Round(1*time.Second).Unix() >= quote.Timestamp.Unix() {
			result = append(result, nil)
			copy(result[1:], result[0:])
			result[0] = message
			howMany--
		}
	}

	return result
}

func filterMsg(messages []VersionedMessage, msgTokens []string) []VersionedMessage {
	if len(msgTokens) == 0 {
		msgTokens = []string{"0"} // single previous message
	}

	var result []VersionedMessage

	parts := make([]interface{}, len(messages))
	for i := range parts {
		parts[i] = messages[i]
	}

	var tmpParts []interface{}

	for _, token := range msgTokens {
		tmpParts = sliceParts(token, parts)
		for i := range tmpParts {
			result = append(result, tmpParts[i].(VersionedMessage))
		}
	}

	return result
}

func filterVer(messages []VersionedMessage, verTokens []string) []VersionedMessage {
	if len(verTokens) == 0 {
		verTokens = []string{"0"} // the very first version
	}

	var result []VersionedMessage

	for _, message := range messages {
		parts := make([]interface{}, len(message))
		for i := range parts {
			parts[i] = message[i]
		}

		for _, token := range verTokens {
			parts = sliceParts(token, parts)
		}

		if len(parts) == 0 {
			continue
		}

		var newMessage VersionedMessage
		for i := range parts {
			newMessage = append(newMessage, parts[i].(*skype.Message))
		}
		result = append(result, newMessage)
	}

	return result
}

func (h History) filter(
	message *skype.Message,
	convToken string, userTokens,
	verTokens, msgTokens []string,
	quote *skype.MsgQuote,
) ([]VersionedMessage, error) {
	var messages []VersionedMessage

	if message.Conv.IsPersonal {
		if len(convToken) == 0 {
			return nil, errors.New(
				"you must specify conversation",
			)
		}

		messages = filterConv(h, convToken, message)
	} else {
		for conv, convMessages := range h {
			if conv.ID == message.Conv.ID {
				messages = append(messages, convMessages...)
			}
		}
	}

	if len(messages) == 0 {
		return nil, errors.New(
			"no messages left after the conversation filtering (no" +
				" conversations were selected or you are not a member of" +
				" the selected conversation)",
		)
	}

	messages = filterUser(messages, userTokens)
	if len(messages) == 0 {
		return nil, errors.New(
			"no messages left after the user filtering (no usernames and" +
				" display names contain your selectors)",
		)
	}

	if quote == nil {
		messages = filterMsg(messages, strFilterEmpty(msgTokens))
		if len(messages) == 0 {
			return nil, errors.New(
				"no messages left after the message filtering (check your" +
					" indices)",
			)
		}
	} else {
		messages = filterMsgQuote(messages, quote)
		if len(messages) == 0 {
			return nil, errors.New(
				"no messages left after the message filtering",
			)
		}
	}

	messages = filterVer(messages, strFilterEmpty(verTokens))
	if len(messages) == 0 {
		return nil, errors.New(
			"no messages left after the message version filtering (check your" +
				" indices)",
		)
	}

	return messages, nil
}
